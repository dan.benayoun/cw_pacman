# Python Pac-Man Clone

This project recreates the classic arcade game Pac-Man. Dive into a world where you navigate mazes, dodge ghosts, and score points, all brought to life through Python and Tkinter and OOP programming

<p align="center">
  <img src="./static/gameImage.jpg" alt="Image Description" />
</p>

## Run file

To run file, simply run the main.py file in src

## Game Mechanics and Features
### Pac-Man

Navigation: Use arrow keys to move Pac-Man through the maze.
Pellets and Power-Ups: Eating pellets increases your score. Power-ups turn ghosts vulnerable for a short duration.
Lives: Start with 3 lives. Losing all lives ends the game.


### Ghosts' AI and Behavior

#### Movement Algorithm 

The ghosts in Pac-Man primarily operate in one of four distinct modes: chase, scatter, frightened, or dead. During the chase mode, which they are in most often, the ghosts determine their target tiles based on Pac-Man's location, though only Blinky directly targets Pac-Man's exact position. In scatter mode, each ghost aims for a specific, unreachable tile, leading them to disperse to the maze's corners and circle around a set area. The frightened mode stands out as the ghosts lose their target tiles and instead make seemingly random choices at intersections. When a ghost is in the dead mode, its goal is the ghost house, where it quickly respawns to regenerate

#### Personalities

Each ghost has a unique chased behavior pattern influencing its movement and interaction with Pac-Man.

##### Red Ghost: Blinky

Blinky's target tile in chase mode is always Pac-Man's current tile. This makes sure that Blinky is usually following close behind the player. He starts outside of the ghost house and is usually the most immediate threat.

##### Pink Ghost: Pinky

He targets the tile a couple of blocks ahead of the direction which Pac-Man is facing.

##### Blue Ghost: Inky

Inky uses both Pac-Man and Blinky's position to select its target tile. To locate Inky's target, we first select the tile two squares ahead of Pac-Man (like Pinky's target). Inky's actual target tile is determined by extending this vector by twice its length, starting from Blinky's position.

##### Orange Ghost: Clyde

Clyde, the orange ghost in the classic Pac-Man game, exhibits a unique behavior that sets him apart from the other ghosts. In his chase mode, Clyde targets Pac-Man directly when he's more than eight tiles away, similar to Blinky's straightforward pursuit. However, when Clyde gets within eight tiles of Pac-Man, he switches tactics and heads towards his designated scatter mode corner in the bottom-left of the maze. 


### Technical Aspects

Implemented in Python, leveraging object-oriented programming principles.
Uses Tkinter for rendering the game interface and handling user inputs.


### Structuration MVP

#### 1. Basic Game Interface and Movement Mechanics
- Develop a basic user interface, such as a grid for the Pac-Man maze.
- Implement Pac-Man's movement controls for up, down, left, and right navigation.

#### 2. Adding the Maze Layout
- Design the maze with walls and pathways.
- Ensure Pac-Man's movement aligns with the maze structure.

#### 3. Incorporating Dots and Power Pellets
- Place dots across the maze for collection.
- Add power pellets that allow Pac-Man to eat ghosts.

#### 4. Implementing Ghost Characters
- Introduce the four ghosts (Blinky, Pinky, Inky, Clyde).
- Start with basic random movement for the ghosts.
- Add ghost/Pacman interaction

#### 5. Developing Ghost AI and Modes
- Program distinct behaviors for each ghost (Chase, Scatter, Frightened, Dead).
- Implement mode-switching logic based on game events.

#### 6. Scoring System and Game Progression
- Create a scoring system for pellet collection and eating ghosts.

#### 7. Adding Game UI Elements
- Include UI elements like score, lives counter.

#### To Do
##### Introducing Special Items and Features
- Include bonus items like fruits.
- Consider new features or gameplay variations.
- Implement different level with increased difficulty

##### Game Over and Restart Mechanics
- Implement game over and restart logic.
- Provide options to restart or return to the main menu.


### TODO List

- [x] Add score display
    - [ ] Make score display prettier
- [ ] Fix movement when trying to go in an impossible direction, keep previous direction and try new direction when possible
- [X] Add all ghost behavior 
    - [X] Inky 
    - [X] Pinky
    - [X] Blinky 
    - [X] Clyde
- [X] Add Scatter behavior 
Changes between Chase and Scatter modes occur on a fixed timer, which causes the "wave" effect described by Iwatani. This timer is reset at the beginning of each level and whenever a life is lost. The timer is also paused while the ghosts are in Frightened mode, which occurs whenever Pac-Man eats an energizer. When Frightened mode ends, the ghosts return to their previous mode, and the timer resumes where it left off. The ghosts start out in Scatter mode, and there are four waves of Scatter/Chase alternation defined, after which the ghosts will remain in Chase mode indefinitely (until the timer is reset). For the first level, the durations of these phases are:
- Scatter for 7 seconds, then Chase for 20 seconds.
- Scatter for 7 seconds, then Chase for 20 seconds.
- Scatter for 5 seconds, then Chase for 20 seconds.
- Scatter for 5 seconds, then switch to Chase mode permanently.
- [X] Fix maze display
- [X] Change ghost / pacman / pellets / power pellets display with jpg 
    - [X] Chnage the display for pacman and ghost according to the facing direction
- [X] Add lives feature
- [X] Add power pellets interaction
    - [X] Add power pellets colision check
    - [X] Add timer for power up duration
    - [X] Add FRIGHTENED behavior for ghost when power pellets eaten
- [ ] Add scoreboard 
- [ ] Add levels with differents map
