import tkinter as tk
from window import Window

def select_option(event):
    global selected
    if event.keysym == "Up":
        if selected > 0:
            selected -= 1
    elif event.keysym == "Down":
        if selected < len(options) - 1:
            selected += 1
    update_options()

def update_options():
    global selected
    for index, label in enumerate(option_labels):
        if index == selected:
            label.config(bg="gray", fg="white", font=("Arial", 12, "bold"))
        else:
            label.config(bg="white", fg="black", font=("Arial", 12))

def start_game(event):
    global selected
    if selected == 0:
        root.deiconify()
        fen.destroy()
        
    elif selected == 1:
        fen.destroy()

if __name__ == '__main__':
    root = tk.Tk()
    root.withdraw()
    fen = tk.Tk()
    fen.title("PaCSman")
    fen.config(bg="blue")
    fen.bind("<Up>", select_option)
    fen.bind("<Down>", select_option)
    fen.bind("<Return>", start_game)

    t = tk.Label(fen, text="PaCSman", bg="blue", fg="red", font=("Arial", 16))
    t.pack()

    options = ["Jouer", "Quitter"]
    selected = 0
    option_labels = []

    for index, option in enumerate(options):
        label = tk.Label(fen, text=option, bg="white", fg="black", font=("Arial", 12))
        label.pack(pady=5)
        option_labels.append(label)
    update_options()
    pacman = Window(root)
    pacman.draw_board()
    pacman.run()
    fen.mainloop()
