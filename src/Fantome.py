import random
from collections import deque
import threading
import time

class Fantome:
    def __init__(self, x, y, root, grid, pacman_x, pacman_y, pacman_invulnerable, color, origin, original_color, pacman_lives, state = "scatter"):
        self.x = x
        self.y = y
        self.direction = "Left"  # Direction initiale du fantôme
        self.root = root
        self.grid = grid
        self.pacman_x = pacman_x
        self.pacman_y = pacman_y
        self.pacman_invulnerable = pacman_invulnerable
        self.color = color
        self.origin = origin
        self.original_color = original_color
        self.state = state
        self.i_blue = 0
        self.i_red = 0
        self.i_orange = 0
        self.i_pink = 0
        self.pacman_lives = pacman_lives

    # Utility methods
    def warp(self):
        if self.x == 15 and self.y == 29:
            self.x = 15 
            self.y = 0
        elif self.x == 15 and self.y == 0:
            self.x = 15
            self.y = 29

    def collision_check(self):
        if self.pacman_x == self.x and self.pacman_y == self.y:
            if self.pacman_invulnerable:
                self.return_to_origin()
            if self.pacman_lives==1:    
                    self.root.destroy()
            else:
                self.pacman_lives-=1
                self.return_to_origin()
                self.pacman_x,self.pacman_y=18,15
                time.sleep(1)

    def distance(self, point1, point2):
        return abs(point1[0] - point2[0]) + abs(point1[1] - point2[1])
    
    def is_valid_cell(self, board, visited, row, col):
        """
        Check if a cell is a valid cell to move to.

        Args:
            board (list): The grid representing the game board.
            visited (list): The visited cells on the game board.
            row (int): The row index of the cell.
            col (int): The column index of the cell.

        Returns:
            bool: True if the cell is valid, False otherwise.
        """
        rows = len(board)
        cols = len(board[0])
        return (
            (row >= 0)
            and (row < rows)
            and (col >= 0)
            and (col < cols)
            and (board[row][col] in [0, 1, 2, 9])
            and not visited[row][col]
        )
    
    def move_to_next(self, next_move):
        """
        Move the Fantome to the next position.

        Args:
            next_move (tuple): The coordinates of the next position (x, y).
        """
        if next_move[0] < self.x:
            self.x -= 1
            self.direction = "Up"
        elif next_move[0] > self.x:
            self.x += 1
            self.direction = "Down"
        elif next_move[1] < self.y:
            self.y -= 1
            self.direction = "Left"
        elif next_move[1] > self.y:
            self.y += 1
            self.direction = "Right"

    def get_possible_directions(self, grid, x, y):
        """
        Get the possible directions to move from the current position.

        Args:
            grid (list): The grid representing the game board.
            x (int): The x-coordinate of the current position.
            y (int): The y-coordinate of the current position.

        Returns:
            list: The possible directions to move.
        """
        directions = {
            'Up': (x, y - 1),
            'Down': (x, y + 1),
            'Left': (x - 1, y),
            'Right': (x + 1, y)
        }
        possible_directions = []
        opposite_direction = {'Up': 'Down', 'Down': 'Up', 'Left': 'Right', 'Right': 'Left'}

        for direction, (dx, dy) in directions.items():
            if direction != opposite_direction[self.direction] and 0 <= dx < len(grid) and 0 <= dy < len(grid[0]) and grid[dx][dy] in [0, 1, 2]:
                possible_directions.append(direction)
        
        return possible_directions

    def return_to_origin(self):
        """
        Move the Fantome back to its original position.
        """
        self.x = self.origin[0]
        self.y = self.origin[1]

    def end_frightened(self):
        self.state = 'scatter'
        self.color = self.original_color


    # Movement methods
    def move_frightened(self, grid):
        """
        Move the Fantome randomly when in the frightened state.

        Args:
            grid (list): The grid representing the game board.
        """

        self.collision_check()
        self.warp()

        possible_directions = self.get_possible_directions(grid, self.x, self.y)

        # If the ghost is at an intersection, choose a new direction
        if self.is_intersection(grid, self.x, self.y):
            new_direction = random.choice(possible_directions)
            self.direction = new_direction
            self.move_in_direction(self.direction)
        
        else:
            # Continue in the current direction if it's possible
            if self.direction in possible_directions:
                self.move_in_direction(self.direction)     
            else: 
                new_direction = random.choice(possible_directions)
                self.direction = new_direction
                self.move_in_direction(self.direction)

    def move_chase_red(self):
        """
        Move the Fantome towards Pac-Man's position using the BFS algorithm.
        """

        self.collision_check()
        self.warp()

        path_to_pacman = self.bfs(self.grid, (self.x, self.y), (self.pacman_x, self.pacman_y))
        if path_to_pacman:
            next_move = path_to_pacman[1]
            if next_move[0] < self.x:
                self.direction = "Left"
                self.x -= 1
            elif next_move[0] > self.x:
                self.x += 1
                self.direction = "Right"
            elif next_move[1] < self.y:
                self.direction = "Up"
                self.y -= 1
            elif next_move[1] > self.y:
                self.y += 1
                self.direction = "Down"

 
    def move_chase_orange(self):

        self.collision_check()
        self.warp()

        if self.distance_pacman_ghost() > 8:
            self.move_chase_red()
        else:
            self.move_scatter_orange(self.grid)
    
    def move_chase_pink(self, pacman_direction):
        """
        Move the Fantome towards Pinky's target position based on Pac-Man's position and direction.

        Args:
            pacman_direction (str): The direction of Pac-Man.

        """

        self.collision_check()
        self.warp()

        # Calculate Pinky's target based on Pac-Man's position and direction

        target_x, target_y = self.pacman_x, self.pacman_y

        if pacman_direction == "Up":
            target_x -= 4  # Four tiles up
        elif pacman_direction == "Down":
            target_x += 4  # Four tiles down
        elif pacman_direction == "Left":
            target_y -= 4  # Four tiles left
        elif pacman_direction == "Right":
            target_y += 4  # Four tiles right

        # Use the BFS algorithm to find the path to the target
        path_to_target = self.bfs(self.grid, (self.x, self.y), (target_x, target_y))
        if path_to_target and len(path_to_target) > 1:
            next_move = path_to_target[1]
            self.move_to_next(next_move)

    def move_chase_blue(self, red_x, red_y, pacman_direction):
        """
        Move blue ghost towards a calculated target position.

        Args:
            red_x (int): The x-coordinate of Blinky's position.
            red_y (int): The y-coordinate of Blinky's position.
            pacman_direction (str): Pac-Man's current direction ('Up', 'Down', 'Left', 'Right').
        """

        self.collision_check()
        self.warp()

        # Calculate the tile two spaces in front of Pac-Man
        target_x, target_y = self.pacman_x, self.pacman_y
        if pacman_direction == "Up":
            # Special case for upward direction
            target_x -= 2
            target_y -= 2
        elif pacman_direction == "Down":
            target_x += 2
        elif pacman_direction == "Left":
            target_y -= 2
        elif pacman_direction == "Right":
            target_y += 2

        # Calculate Inky's target by doubling the vector from Blinky's position
        inky_target_x = 2 * target_x - red_x
        inky_target_y = 2 * target_y - red_y

        # Use the BFS algorithm to find the path to Inky's target
        path_to_target = self.bfs(self.grid, (self.x, self.y), (inky_target_x, inky_target_y))
        if path_to_target and len(path_to_target) > 1:
            next_move = path_to_target[1]
            self.move_to_next(next_move)

    def move_scatter_orange(self,board):

        self.collision_check()
        self.warp()

        lst=[(27,2),(30,13),(25,10)]
        n=3

        l=self.bfs(board, (self.x,self.y), lst[self.i_orange])
        if l:
            if len(l)>1:
                next_move = l[1]
                if next_move[0] < self.x:
                    self.x -= 1
                elif next_move[0] > self.x:
                    self.x += 1
                elif next_move[1] < self.y:
                    self.y -= 1
                elif next_move[1] > self.y:
                    self.y += 1
            else:
                if self.i_orange<n-1:   
                    self.i_orange+=1
                else:
                    self.i_orange=0       
        
    def move_scatter_blue(self,board):

        self.collision_check()
        self.warp()

        lst=[(27,27),(30,16),(24,20)]
        n=3
        
        l=self.bfs(board, (self.x,self.y), lst[self.i_blue])
        if l:
            if len(l)>1:
                next_move = l[1]
                if next_move[0] < self.x:
                    self.x -= 1
                elif next_move[0] > self.x:
                    self.x += 1
                elif next_move[1] < self.y:
                    self.y -= 1
                elif next_move[1] > self.y:
                    self.y += 1
            else:
                if self.i_blue<n-1:   
                    self.i_blue+=1
                else:
                    self.i_blue=0
            
    def move_scatter_pink(self,board):

        self.collision_check()
        self.warp()

        lst=[(2,2),(6,5),(2,7)]
        n=3

        l=self.bfs(board, (self.x,self.y), lst[self.i_pink])
        if l:
            if len(l)>1:
                next_move = l[1]
                if next_move[0] < self.x:
                    self.x -= 1
                elif next_move[0] > self.x:
                    self.x += 1
                elif next_move[1] < self.y:
                    self.y -= 1
                elif next_move[1] > self.y:
                    self.y += 1
            else:
                if self.i_pink<n-1:   
                    self.i_pink+=1
                else:
                    self.i_pink=0
    
    def move_scatter_red(self, board):

        self.collision_check()
        self.warp()

        lst = [(2, 27), (4, 27), (6, 22)]
        n = 3

        l = self.bfs(board, (self.x, self.y), lst[self.i_red])
        if l:
            if len(l) > 1:
                next_move = l[1]
                if next_move[0] < self.x:
                    self.x -= 1
                elif next_move[0] > self.x:
                    self.x += 1
                elif next_move[1] < self.y:
                    self.y -= 1
                elif next_move[1] > self.y:
                    self.y += 1
            else:
                if self.i_red < n - 1:
                    self.i_red += 1
                else:
                    self.i_red = 0

    # Pathfinding methods

    def bfs(self, board, start, end):
        """
        Perform a Breadth-First Search (BFS) algorithm to find the shortest path from the start position to the end position on the game board.

        Args:
            board (list): The grid representing the game board.
            start (tuple): The start position (x, y).
            end (tuple): The end position (x, y).

        Returns:
            list: The shortest path from the start position to the end position.
        """
        rows = len(board)
        cols = len(board[0])
        visited = [[False for _ in range(cols)] for _ in range(rows)]
        prev = [[None for _ in range(cols)] for _ in range(rows)]

        queue = deque()
        queue.append(start)
        visited[start[0]][start[1]] = True

        closest = start
        min_dist = self.distance(start, end)

        while queue:
            current = queue.popleft()
            dist_to_end = self.distance(current, end)

            if dist_to_end < min_dist:
                closest = current
                min_dist = dist_to_end

            for direction in [[-1, 0], [0, -1], [0, 1], [1, 0]]:
                next_row = current[0] + direction[0]
                next_col = current[1] + direction[1]

                if self.is_valid_cell(board, visited, next_row, next_col):
                    queue.append((next_row, next_col))
                    visited[next_row][next_col] = True
                    prev[next_row][next_col] = current

        path = []
        current = closest
        while current != start:
            path.append(current)
            current = prev[current[0]][current[1]]
        path.append(start)
        return path[::-1]

    def distance_pacman_ghost(self):
        return len(self.bfs(self.grid, (self.x, self.y), (self.pacman_x, self.pacman_y)))

    def is_intersection(self, grid, x, y):
        """
        Check if a cell is an intersection.

        Args:
            grid (list): The grid representing the game board.
            x (int): The x-coordinate of the cell.
            y (int): The y-coordinate of the cell.

        Returns:
            bool: True if the cell is an intersection, False otherwise.
        """
        directions = ['Up', 'Down', 'Left', 'Right']
        opposite_direction = {'Up': 'Down', 'Down': 'Up', 'Left': 'Right', 'Right': 'Left'}
        
        # Exclude the reverse direction
        directions.remove(opposite_direction[self.direction])
        
        free_paths = 0

        # Check the cells in all possible directions
        if y > 0 and (grid[x][y - 1] == 1 or grid[x][y - 1] == 2 or grid[x][y - 1] == 0) and self.direction != 'Down':  # Check Up
            free_paths += 1
        if y < len(grid[0]) - 1 and (grid[x][y + 1] == 1 or grid[x][y + 1] == 2 or grid[x][y + 1] == 0) and self.direction != 'Up':  # Check Down
            free_paths += 1
        if x > 0 and (grid[x - 1][y] == 1 or grid[x - 1][y] == 2 or grid[x - 1][y] == 0) and self.direction != 'Right':  # Check Left
            free_paths += 1
        if x < len(grid) - 1 and (grid[x + 1][y] == 1 or grid[x + 1][y] == 2 or grid[x + 1][y] == 0) and self.direction != 'Left':  # Check Right
            free_paths += 1

        # If there are more than two free paths, it's an intersection
        if free_paths > 2:
            return True
        else:
            return False

    def classify_intersection(self, grid, x, y):
        """
        Classify the type of intersection.

        Args:
            grid (list): The grid representing the game board.
            x (int): The x-coordinate of the intersection.
            y (int): The y-coordinate of the intersection.

        Returns:
            list: The classification of the intersection.
        """
        intersection = [0,0,0,0,0]
        directions = {
            'Up': (x, y - 1),
            'Down': (x, y + 1),
            'Left': (x - 1, y),
            'Right': (x + 1, y)
        }
        open_directions = []

        for direction, (dx, dy) in directions.items():
            if 0 <= dx < len(grid) and 0 <= dy < len(grid[0]) and ( grid[dx][dy] == 0 or grid[dx][dy] == 1 or grid[dx][dy] == 2):
                open_directions.append(direction)

        # Classify the type of intersection, intersection[0] for 'T-intersection facing Down', intersection[1] for 'T-intersection facing Up', intersection[2] for 'T-intersection facing Right', intersection[3] for 'T-intersection facing Left', intersection[4] for '4-way intersection'
        if len(open_directions) == 3:
            if 'Up' not in open_directions:
                intersection[0] = 1
            elif 'Down' not in open_directions:
                intersection[1] = 1
            elif 'Left' not in open_directions:
                intersection[2] = 1
            elif 'Right' not in open_directions:
                intersection[3] = 1
        elif len(open_directions) > 3:
            intersection[4] = 1

    # Else

    def move_in_direction(self, direction):
        if direction == 'Up':
            self.y -= 1
        elif direction == 'Down':
            self.y += 1
        elif direction == 'Left':
            self.x -= 1
        elif direction == 'Right':
            self.x += 1

    def frightened_state(self):
        self.state = 'frightened'
        self.color = 'frightened'
        # Start a timer to end frightened state after 10 seconds
        threading.Timer(10, self.end_frightened).start()


