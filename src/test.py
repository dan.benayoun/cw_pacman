from Fantome import *
from Pacman import *
board = [
    [0, 0, 0, 0],
    [1, 1, 0, 1],
    [0, 0, 0, 0],
    [0, 4, 1, 9],]


def test_draw_board():
    cnv = Canvas()
    p = Pacman(x=1, y=1)
    f1 = Fantome(x=2, y=2)
    f2 = Fantome(x=3, y=3)
    draw_board(cnv, p, f1, f2, board)
    assert cnv.find_all()

#(pas de test pour update_game car elle appelle juste l'autre fonction ?)


def test_move_aleatoire():
    ghost=Fantome(x=1,y=1)
    ghost.move()
    if  self.direction =='up':
        assert self.x==0
        assert self.y==1
    elif  self.direction =='down':
        assert self.x==2
        assert self.y==1
    elif  self.direction =='left':
        assert self.x==1
        assert self.y==0
    elif  self.direction =='right':
        assert self.x==1
        assert self.y==2


board = [
    [0, 0, 0, 0],
    [1, 1, 0, 1],
    [0, 0, 0, 0],
    [0, 4, 1, 9]]

def test_is_valid_cell_valid():
    visited = [[False, False, False, False],
        [False, False, False, False],
        [False, False, False, False],
        [False, False, False, False]]
    row, col = 1, 2
    assert is_valid_cell(board, visited, row, col) == True
def test_is_valid_cell_invalid_out_of_bounds():
    visited = [[False, False, False, False],
        [False, False, False, False],
        [False, False, False, False],
        [False, False, False, False]]
    row, col = 4, 2
    assert is_valid_cell(board, visited, row, col) == False
def test_is_valid_cell_invalid_value():
    visited = [[False, False, False, False],
        [False, False, False, False],
        [False, False, False, False],
        [False, False, False, False]]
    row, col = 1, 1  # Cette cellule a la valeur 1, ce qui est invalide
    assert is_valid_cell(board, visited, row, col) == False
def test_is_valid_cell_invalid_already_visited():
    visited = [[False, False, False, False],
        [False, False, False, False],
        [False, False, True, False],
        [False, False, False, False]]
    row, col = 2, 2
    assert is_valid_cell(board, visited, row, col) == False


def test_bfs_valid_path():
    start = (0, 0)
    end = (3, 3)
    path = bfs(board, start, end)
    assert path == [(0, 0), (1, 0), (1, 1), (1, 2), (2, 2), (3, 2), (3, 3)]
def test_bfs_no_path():
    board = [
        [0, 0, 0, 0],
        [1, 1, 1, 1],
        [0, 0, 0, 0],
        [0, 1, 1, 9],
    ]
    start = (0, 0)
    end = (3, 0)
    path = bfs(board, start, end)
    assert path is None


def test_move_fantome():
    pacman = Pacman(x=3, y=3)
    ghost = Ghost(x=1, y=1)
    ghost.move()
    assert ghost.x in [1, 2]
    assert ghost.y == 1


def test_manhattan_distance():
    assert manhattan_distance ((3,5),(2,1))==5



    
