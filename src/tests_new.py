
from Fantome import Fantome
import pytest 
from Pacman import Pacman
import tkinter as tk
from window import Window
import random
from collections import deque
import threading
from board import Board
from unittest.mock import patch

#==========================================-tests class Pacman-==========================================
b = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
]
def pacman_instance():
    return Pacman(1,1,0,b)

def test_move(pacman_instance):
    #Exemple de board pour test
    pacman_instance.board = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    # position initiale
    pacman_instance.x = 2
    pacman_instance.y = 2
    # Test move Right
    pacman_instance.move("Right")
    assert pacman_instance.y == 3
    # Test move Left
    pacman_instance.move("Left")
    assert pacman_instance.y == 2
    # Test move Up
    pacman_instance.move("Up")
    assert pacman_instance.x == 1
    # Test move Down
    pacman_instance.move("Down")
    assert pacman_instance.x == 2


def test_eat_power_pellet(pacman_instance):
    pacman_instance.board = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    pacman_instance.eat_power_pellet()
    assert pacman_instance.invulnerable == True


def test_end_invulnerability():
    pacman = Pacman(1,1,0,b)
    pacman.eat_power_pellet()
    pacman.end_invulnerability()
    assert pacman.invulnerable == False


def test_end_invulnerability(pacman_instance):
    pacman_instance.invulnerable = True

    pacman_instance.end_invulnerability()
    assert pacman_instance.invulnerable == False


#==========================================-tests class Fantome-==========================================

def instance():
    return Fantome(15,17, 'root', b, 2, 2, False, 3, "orange", (15, 17), "orange")

def test_move_aleatoire(instance):
    instance.grid = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    #position initiale
    instance.x = 2
    instance.y = 2

    instance.direction = 'InitialDirection'
    with patch('random.choice', return_value='Up'):
        instance.move_aleatoire()

        assert instance.x == 1
        assert instance.direction == 'Up'


def test_bfs(graph):
    # Example de board 
    board = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    start = (0, 0)
    end = (4, 4)
    result = graph.bfs(board, start, end)
    expected_path = [(0, 0), (1, 0), (2, 0), (2, 1), (2, 2), (2, 3), (3, 3), (4, 4)]
    assert result == expected_path


def test_is_valid_cell(graph):
    # Example de board 
    board = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    visited = [
        [False, False, False, False, False],
        [False, False, False, False, False],
        [False, False, False, False, False],
        [False, False, False, False, False],
        [False, False, False, False, False]
    ]
    # Test valid cell
    assert graph.is_valid_cell(board, visited, 1, 1) == True

    # Test invalid cell 
    assert graph.is_valid_cell(board, visited, 0, 0) == False

    # Test invalid cell 
    assert graph.is_valid_cell(board, visited, -1, 1) == False

    # Test invalid cell 
    assert graph.is_valid_cell(board, visited, 1, 2) == False


def test_return_to_origin(instance):
    # position initiale
    instance.x = 3
    instance.y = 5
    instance.return_to_origin()
    assert instance.x == instance.origin[0]
    assert instance.y == instance.origin[1]


def test_move_chase_red(instance):
    # positions initiales
    instance.x = 3
    instance.y = 5
    instance.pacman_x = 1
    instance.pacman_y = 1

    instance.grid = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    instance.pacman_invulnerable = False
    instance.move_chase_red()
    assert instance.x == 4
    assert instance.y == 5
    assert instance.direction == "Right"


def test_is_intersection(instance):
    grid = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    instance.direction = 'Right'
    assert instance.is_intersection(grid, 1, 1) == False
    assert instance.is_intersection(grid, 2, 1) == True


def test_get_possible_directions(instance):
    grid = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    instance.direction = 'Right'
    assert instance.get_possible_directions(grid, 1, 1) == ['Up', 'Down', 'Right']
    assert instance.get_possible_directions(grid, 0, 0) == []


def test_move_in_direction(instance):
    #position initiale
    instance.x = 3
    instance.y = 5
    # Test move 'Up'
    instance.move_in_direction('Up')
    assert instance.x == 3
    assert instance.y == 4
    # Test move 'Down'
    instance.move_in_direction('Down')
    assert instance.x == 3
    assert instance.y == 5
    # Test move 'Left'
    instance.move_in_direction('Left')
    assert instance.x == 2
    assert instance.y == 5
    # Test move 'Right'
    instance.move_in_direction('Right')
    assert instance.x == 3
    assert instance.y == 5



def test_distance_pacman_ghost(instance):
    instance.grid = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    instance.x = 3
    instance.y = 5
    instance.pacman_x = 1
    instance.pacman_y = 1
    instance.bfs = lambda grid, start, end: [(3, 5), (2, 5), (1, 5), (1, 4), (1, 3), (1, 2), (1, 1)]
    assert instance.distance_pacman_ghost() == 6


def test_scatter_Sud_Ouest(instance):
    board = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ]
    instance.x = 3
    instance.y = 5
    instance.i_orange = 0

    instance.bfs = lambda board, start, end: [(3, 5), (2, 5), (1, 5), (1, 4), (1, 3), (1, 2), (1, 1)]
    instance.scatter_Sud_Ouest(board)

    assert instance.x == 2
    assert instance.y == 5


def test_frightened_state(instance):
    instance.state = 'normal'
    instance.color = 'red'

    with patch.object(instance, 'end_frightened') as mock_end_frightened:
        instance.frightened_state()
        assert instance.state == 'frightened'
        assert instance.color == 'blue'

#==========================================-tests class window-==========================================





