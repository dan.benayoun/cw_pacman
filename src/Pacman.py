import threading

class Pacman:

    def __init__(self, x, y, score, board):
        self.x = x
        self.y = y
        self.score = score
        self.board = board
        self.lives = 3
        self.invulnerable = False

    def move(self, direction) :
        if direction == "Right" and self.y == 29 and self.x == 15:
            self.y = 0
        elif direction == "Left" and self.y == 0 and self.x == 15:
            self.y = 29    

        elif direction == "Right" and self.y < len(self.board[1]) - 1 :
            if self.board[self.x][self.y + 1] not in [3, 4, 5, 6, 7, 8] :
                self.y += 1

        elif direction == "Left" and self.y  >= 0 :
            if self.board[self.x][self.y - 1] not in [3, 4, 5, 6, 7, 8] :
                self.y -= 1

        elif direction == "Up" and self.x >= 0 :
            if self.board[self.x - 1][self.y] not in [3, 4, 5, 6, 7, 8] :
                self.x -= 1

        elif direction == "Down" and self.x < len(self.board) - 1 :
            if self.board[self.x + 1][self.y] not in [3, 4, 5, 6, 7, 8] :
                self.x += 1

    # Power pellets interaction

    # This is where the Pacman eats a power pellet
    def eat_power_pellet(self):
        # Make Pacman invulnerable
        self.invulnerable = True

        # Start a timer to end invulnerability after 10 seconds
        threading.Timer(10, self.end_invulnerability).start()

    # This method will be called after the timer ends
    def end_invulnerability(self):
        self.invulnerable = False

    # Display function 

    def display_score(self):
        return f'Score: {self.score}'
    
    def display_lives(self):
        return f'Vies restantes: {self.lives}'

    # Add direction_image depending on the direction of the pacman
