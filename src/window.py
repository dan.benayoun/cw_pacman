import tkinter as tk
from board import Board
#import winsound
from PIL import Image, ImageTk
import time
import threading
file_path="./static/pacman_chomp.wav"
class Window():

    def __init__(self, root):
        self.root = root
        self.row = 25
        self.cols = 25
        self.CELL_SIZE = 30
        self.hauteur = self.row * self.CELL_SIZE

        # Tkinter settings
        self.canvas = tk.Canvas(root, width=self.cols * self.CELL_SIZE, height=self.row * self.CELL_SIZE, bg="black")
        self.canvas.pack(side=tk.LEFT)  # Display the canvas on the left side

        self.score_label = tk.Label(self.root, text='0', font=('Arial', 20), bg="black", fg="white", bd=0)  # Set the background color of the label to black and remove the border
        self.vies_label=tk.Label(self.root,text='3',font=('Arial', 20), bg="black", fg="red", bd=0)
        self.vies_label.pack(side=tk.TOP)
        self.score_label.pack(side=tk.TOP)  # Display the score label on the left side

        self.pacman_images = {
            "Right": self.load_image("./static/pacmanDroite.png"),
            "Left": self.load_image("./static/pacmanGauche.png"),
            "Up": self.load_image("./static/pacmanHaut.png"),
            "Down": self.load_image("./static/pacmanBas.png"),
            "frightened": self.load_image("./static/frightened.png")
        }
        self.current_pacman_image = self.pacman_images["Right"]

        self.red_image = self.load_ghost_image("./static/blinky-red_Left.png")  
        self.pink_image = self.load_ghost_image("./static/pinky-pink_Left.png")  
        self.blue_image = self.load_ghost_image("./static/inky-blue_Left.png") 
        self.orange_image = self.load_ghost_image("./static/clive-orange_Left.png")
        self.frightened_image = self.load_ghost_image("./static/frightened.png")

        # Pacman board initialized 

        self.board = Board(self.row, self.cols, self.root)

    # load images
    def load_image(self, path):
        image = Image.open(path)
        image = image.resize((self.CELL_SIZE, self.CELL_SIZE))
        return ImageTk.PhotoImage(image)

    #Drawing the board
    def draw_pacman(self, n1, n2):
        self.canvas.create_image(
            (self.board.pacman.y + 0.5) * n2,
            (self.board.pacman.x + 0.5) * n1,
            anchor=tk.CENTER,
            image=self.current_pacman_image
        )
    def load_ghost_image(self, path):
        ghost_image = Image.open(path)
        ghost_image = ghost_image.resize((self.CELL_SIZE, self.CELL_SIZE))
        return ImageTk.PhotoImage(ghost_image)

    def draw_ghost(self, ghost, n1, n2):
        if ghost.color == "frightened":
            ghost_image = self.frightened_image
        if ghost.color == "pink" :
            ghost_image = self.pink_image 
        if ghost.color == "red" :
            ghost_image = self.red_image  
        if ghost.color == "blue" :
            ghost_image = self.blue_image  
        if ghost.color == "orange" :
            ghost_image = self.orange_image 
        self.canvas.create_image(
            (ghost.y + 0.5) * n2,
            (ghost.x + 0.5) * n1,
            anchor=tk.CENTER,
            image=ghost_image
            )

    def draw_board(self):
        self.canvas.delete("all")
        n1 = (self.hauteur - 50) // 32
        n2 = self.hauteur // 30

        # Drawing Pacman // A remplacer par une image
        self.draw_pacman(n1,n2)

        # Drawing Ghosts // A remplacer par une image
        self.draw_ghost(self.board.red, n1, n2)
        self.draw_ghost(self.board.pink, n1, n2)
        self.draw_ghost(self.board.blue, n1, n2)
        self.draw_ghost(self.board.orange, n1, n2)

        # Drawing the board
        for i in range(len(self.board.grid)):
            for j in range(len(self.board.grid[i])):
                if self.board.grid[i][j] == 1:
                    self.canvas.create_oval(
                        (j + 0.5) * n2 - 5,
                        (i + 0.5) * n1 - 5,
                        (j + 0.5) * n2 + 5,
                        (i + 0.5) * n1 + 5,
                        fill="white",
                    )
                if self.board.grid[i][j] == 2:
                    self.canvas.create_oval(
                        (j + 0.5) * n2 - 7.5,
                        (i + 0.5) * n1 - 7.5,
                        (j + 0.5) * n2 + 7.5,
                        (i + 0.5) * n1 + 7.5,
                        fill="white",
                    )
                if self.board.grid[i][j] == 3:
                    self.canvas.create_line(
                        (j + 0.5) * n2,
                        i * n1,
                        (j + 0.5) * n2,
                        i * n1 + n1,
                        fill="blue",
                        width=1.5,
                    )
                if self.board.grid[i][j] == 4:
                    self.canvas.create_line(
                        j * n2,
                        (i + 0.5) * n1,
                        (j + 1) * n2,
                        (i + 0.5) * n1,
                        fill="blue",
                        width=1.5,
                    )

                if self.board.grid[i][j] == 5:
                    self.canvas.create_arc(
                        (j * n2 - (n2 * 0.44)) - 2,
                        (i * n1 + (0.49 * n1)),
                        (j * n2 - (n2 * 0.44)) - 2 + n2,
                        (i * n1 + (0.49 * n1)) + n1,
                        start=0,
                        extent=90,
                        outline="blue",
                        width=1.5,
                        style=tk.ARC,
                    )
                if self.board.grid[i][j] == 6:
                    self.canvas.create_arc(
                        (j * n2 + (n2 * 0.5)),
                        (i * n1 + (0.5 * n1)),
                        (j * n2 + (n2 * 0.5)) + n2,
                        (i * n1 + (0.5 * n1)) + n1,
                        start=90,
                        extent=90,
                        outline="blue",
                        width=1.5,
                        style=tk.ARC,
                    )
                if self.board.grid[i][j] == 7:
                    self.canvas.create_arc(
                        (j * n2 + (n2 * 0.48)),
                        (i * n1 - (0.52 * n1)),
                        (j * n2 + (n2 * 0.48)) + n2,
                        (i * n1 - (0.52 * n1)) + n1,
                        start=180,
                        extent=90,
                        outline="blue",
                        width=1.5,
                        style=tk.ARC,
                    )
                if self.board.grid[i][j] == 8:
                    self.canvas.create_arc(
                        (j * n2 - (n2 * 0.43)) - 2,
                        (i * n1 - (0.505 * n1)),
                        (j * n2 - (n2 * 0.43)) - 2 + n2,
                        (i * n1 - (0.505 * n1)) + n1,
                        start=270,
                        extent=90,
                        outline="blue",
                        width=1.5,
                        style=tk.ARC,
                    )
                if self.board.grid[i][j] == 9:
                    self.canvas.create_line(
                        (j) * n2,
                        (i + 0.5) * n1,
                        (j + 1) * n2,
                        (i + 0.5) * n1,
                        fill="white",
                        width=1.5,
                    )

    def draw_stats(self):
        self.score_label['text'] = self.board.pacman.display_score()
        self.vies_label['text']=self.board.pacman.display_lives()
        self.canvas.update()
    
    def update_pacman_pos(self):
        self.board.red.pacman_x = self.board.pacman.x
        self.board.pink.pacman_x = self.board.pacman.x
        self.board.blue.pacman_x = self.board.pacman.x
        self.board.orange.pacman_x = self.board.pacman.x
        self.board.red.pacman_y = self.board.pacman.y
        self.board.pink.pacman_y= self.board.pacman.y
        self.board.blue.pacman_y = self.board.pacman.y
        self.board.orange.pacman_y = self.board.pacman.y
        self.board.red.pacman_invulnerable = self.board.pacman.invulnerable
        self.board.pink.pacman_invulnerable = self.board.pacman.invulnerable
        self.board.blue.pacman_invulnerable = self.board.pacman.invulnerable
        self.board.orange.pacman_invulnerable = self.board.pacman.invulnerable  
        self.board.red.pacman_lives = self.board.pacman.lives
        self.board.pink.pacman_lives = self.board.pacman.lives
        self.board.blue.pacman_lives = self.board.pacman.lives
        self.board.orange.pacman_lives = self.board.pacman.lives                      
    
    def pacman_eat_ghost(self):
        if self.board.pacman.x == self.board.red.x and self.board.pacman.y == self.board.red.y:
            if self.board.pacman.invulnerable:
                self.board.red.x = self.board.red.origin[0]
                self.board.red.y = self.board.red.origin[1]
                self.board.pacman.score += 200
                self.draw_stats()
            else :
                if self.board.pacman.lives==1:
                    self.root.destroy()
                    
                else:
                    self.board.pacman.lives-=1
                    self.board.blue.return_to_origin()
                    self.board.pink.return_to_origin()
                    self.board.orange.return_to_origin()
                    self.board.red.return_to_origin()
                    self.board.blue.state = "scatter"
                    self.board.pink.state = "scatter"
                    self.board.orange.state = "scatter"
                    self.board.red.state = "scatter"
                    self.board.pacman.x,self.board.pacman.y=18,15
                    time.sleep(1)
        if self.board.pacman.x == self.board.pink.x and self.board.pacman.y == self.board.pink.y:
            if self.board.pacman.invulnerable:
                self.board.pink.x = self.board.pink.origin[0]
                self.board.pink.y = self.board.pink.origin[1]
                self.board.pacman.score += 200
                self.draw_stats()
            else :
                if self.board.pacman.lives==1:
                    
                    self.root.destroy()
                else:
                    self.board.pacman.lives-=1
                    self.board.blue.return_to_origin()
                    self.board.pink.return_to_origin()
                    self.board.orange.return_to_origin()
                    self.board.red.return_to_origin()
                    self.board.blue.state = "scatter"
                    self.board.pink.state = "scatter"
                    self.board.orange.state = "scatter"
                    self.board.red.state = "scatter"
                    self.board.pacman.x,self.board.pacman.y=18,15
                    time.sleep(1)
        if self.board.pacman.x == self.board.blue.x and self.board.pacman.y == self.board.blue.y:
            if self.board.pacman.invulnerable:
                self.board.blue.x = self.board.blue.origin[0]
                self.board.blue.y = self.board.blue.origin[1]
                self.board.pacman.score += 200
                self.draw_stats()
            else :
                if self.board.pacman.lives==1:
                    
                    self.root.destroy()
                else:
                    self.board.pacman.lives-=1
                    self.board.blue.return_to_origin()
                    self.board.pink.return_to_origin()
                    self.board.orange.return_to_origin()
                    self.board.red.return_to_origin()
                    self.board.blue.state = "scatter"
                    self.board.pink.state = "scatter"
                    self.board.orange.state = "scatter"
                    self.board.red.state = "scatter"
                    self.board.pacman.x,self.board.pacman.y=18,15
                    time.sleep(1)
        if self.board.pacman.x == self.board.orange.x and self.board.pacman.y == self.board.orange.y:
            if self.board.pacman.invulnerable:
                self.board.orange.x = self.board.orange.origin[0]
                self.board.orange.y = self.board.orange.origin[1]
                self.board.pacman.score += 200
                self.draw_stats()
            else :
                if self.board.pacman.lives==1:
                    
                    self.root.destroy()
                else:
                    self.board.pacman.lives-=1
                    self.board.blue.return_to_origin()
                    self.board.pink.return_to_origin()
                    self.board.orange.return_to_origin()
                    self.board.red.return_to_origin()
                    self.board.blue.state = "scatter"
                    self.board.pink.state = "scatter"
                    self.board.orange.state = "scatter"
                    self.board.red.state = "scatter"
                    self.board.pacman.x,self.board.pacman.y=18,15
                    time.sleep(1)

    def ghost_frighened(self):
        self.board.orange.frightened_state()     
        self.board.blue.frightened_state()
        self.board.pink.frightened_state()
        self.board.red.frightened_state()
    
    def pacman_eat_pellet(self):
        if self.board.grid[self.board.pacman.x][self.board.pacman.y] == 1:
            self.board.grid[self.board.pacman.x][self.board.pacman.y] = 0
            self.board.pacman.score += 10
            self.draw_stats()
        if self.board.grid[self.board.pacman.x][self.board.pacman.y] == 2:
            self.board.grid[self.board.pacman.x][self.board.pacman.y] = 0
            self.board.pacman.score += 50
            self.board.pacman.eat_power_pellet()
            self.ghost_frighened()
            self.frightened_start_time = time.time()
            self.draw_stats()

    def move_pacman(self):
        if self.board.current_direction and self.board.moving:
            if self.current_pacman_image != self.pacman_images[self.board.current_direction]:
                self.current_pacman_image = self.pacman_images[self.board.current_direction]
        self.board.pacman.move(self.board.current_direction)
        self.pacman_eat_pellet()
        self.pacman_eat_ghost()         
        self.update_pacman_pos()
        self.pacman_eat_pellet()
        self.pacman_eat_ghost() 
        self.draw_stats()
        self.draw_board()
        self.root.after(90, self.move_pacman) 
    
    def ghost_behavior(self):
        # Start with scatter mode
        self.board.red.state = "scatter"
        self.board.pink.state = "scatter"
        self.board.blue.state = "scatter"
        self.board.orange.state = "scatter"

        if self.board.red.state != "frightened":
            # Schedule the changes in the ghost's state
            self.root.after(7000, lambda: self.change_ghost_state("chase", 20000))
            self.root.after(27000, lambda: self.change_ghost_state("scatter", 7000))
            self.root.after(34000, lambda: self.change_ghost_state("chase", 20000))
            self.root.after(54000, lambda: self.change_ghost_state("scatter", 5000))
            self.root.after(59000, lambda: self.change_ghost_state("chase"))

    def change_ghost_state(self, state, delay=None):
        # Change the ghost's state
        self.board.red.state = state
        self.board.pink.state = state
        self.board.blue.state = state
        self.board.orange.state = state

        # If a delay is provided, schedule the next change in the ghost's state
        if delay is not None:
            self.root.after(delay, lambda: self.change_ghost_state("scatter" if state == "chase" else "chase", 20000 if state == "chase" else 5000))

    def eat_power_pellet(self):
        # Make Pacman invulnerable
        self.board.pacman.invulnerable = True
        # Start a timer to end invulnerability after 10 seconds
        threading.Timer(10, self.end_invulnerability).start()

    # This method will be called after the timer ends
    def end_invulnerability(self):
        self.board.pacman.invulnerable = False
        self.ghost_back_to_normal


    def ghost_back_to_normal(self):
        # Calculate the elapsed time in the frightened state
        elapsed_time = time.time() - self.frightened_start_time

        # Subtract the elapsed time from the remaining time in the current mode
        self.remaining_time -= elapsed_time

        # Resume the ghost behavior schedule
        self.frightened = False
        self.ghost_behavior()

    def move_fantome(self):
        self.draw_board()
        if self.board.red.state == "frightened" :
            self.board.red.move_frightened(self.board.grid)
            self.board.pink.move_frightened(self.board.grid)
            self.board.blue.move_frightened(self.board.grid)    
            self.board.orange.move_frightened(self.board.grid)
        elif self.board.red.state == "chase" :
            self.board.red.move_chase_red()
            self.board.pink.move_chase_pink(self.board.current_direction)
            self.board.blue.move_chase_blue(self.board.red.x, self.board.red.y, self.board.current_direction)
            self.board.orange.move_chase_orange()
        elif self.board.red.state == "scatter":           
            self.board.red.move_scatter_red(self.board.grid)
            self.board.pink.move_scatter_pink(self.board.grid)
            self.board.blue.move_scatter_blue(self.board.grid)
            self.board.orange.move_scatter_orange(self.board.grid)
        self.root.after(180, self.move_fantome)

    # def son(self):
    #     winsound.PlaySound(file_path,winsound.SND_LOOP + winsound.SND_ASYNC)

    def update_game(self, event):
        if event.keysym in ['Right', 'Left', 'Up', 'Down']:
            self.board.current_direction = event.keysym
            if not self.board.moving:
                self.board.moving = True
                self.move_pacman()
                self.move_fantome()
                self.son()  

    def run(self):
        self.root.bind('<Key>', self.update_game)
        self.ghost_behavior()
        self.root.mainloop()            